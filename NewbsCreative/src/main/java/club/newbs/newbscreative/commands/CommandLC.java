package club.newbs.newbscreative.commands;

import club.newbs.newbscreative.NewbsCreative;
import club.newbs.newbscreative.api.Regions;
import club.newbs.newbscreative.api.commands.NCommand;
import club.newbs.newbscreative.api.inventory.NInventory;
import club.newbs.newbscreative.api.lc.LCMessages;
import club.newbs.newbscreative.api.lc.LCModes;
import club.newbs.newbscreative.api.restrictions.Blacklist;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.bukkit.ChatColor.*;
import static club.newbs.newbscreative.api.lc.LCMessages.*;

public class CommandLC extends NCommand {

    private final NewbsCreative core;
    private final Regions regions;

    public CommandLC(NewbsCreative core){
        this.core = core;
        this.regions = core.getRegions();
    }

    @Override
    public void command(Player player, String[] args) {
        if(args.length == 0){ lcSelf(player); }
        else if(args.length == 1) {
            if(args[0].equalsIgnoreCase("list")) {
                lcList(player);
            }else if(args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
                lcHelp(player);
            } else if(args[0].equalsIgnoreCase("reload")){
                lcReload(player);
            } else {
                lcOther(player, args); }
        } else if(args.length == 2) {
            if(args[0].equalsIgnoreCase("bans")) { lcBans(player, args); }
        }
        else { lcHelp(player); }
    }

    public void lcSelf(Player p){
        UUID uuid = p.getUniqueId();
        LCModes mode = LCModes.getMode(p);

        if(mode == null){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PERMS));
            return;
        }

        if(core.lc.containsKey(uuid)){
            core.bans.remove(uuid);
            core.lc.remove(uuid);
            if(!(mode == LCModes.ADMIN)){
                NInventory.restoreInventory(p);
            }
            p.setGameMode(GameMode.SURVIVAL);
            p.sendMessage(getCore().getPrefix() + getMessage(LEAVE));
            return;
        }

        if(regions != null && !regions.allow(p)){
            if(mode == LCModes.ADMIN){
                p.sendMessage(getCore().getPrefix() + getMessage(ENTER_BYPASS).replace("%", regions.getDeniedRegion(p).getId().toUpperCase()));
            }else{
                p.sendMessage(getCore().getPrefix() + getMessage(REGION_BLOCKED).replace("%", regions.getDeniedRegion(p).getId().toUpperCase()));
                return;
            }
        }else{
            p.sendMessage(getCore().getPrefix() + getMessage(ENTER));
        }

        NInventory.saveInventory(p);
        if(!(LCModes.getMode(p) == LCModes.ADMIN)){
            p.getInventory().clear();
        }

        core.bans.put(uuid, new Blacklist(Blacklist.loadBans(p)));
        core.lc.put(uuid, mode);
    }

    public void lcOther(Player p, String[] args){
        Player target = Bukkit.getPlayer(args[0]);
        LCModes mode = LCModes.TRIAL;

        if(!p.hasPermission("newbs.creative.others")){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PERMS));
            return;
        }
        if(target == null){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PLAYER));
            return;
        }
        UUID uuid = target.getUniqueId();

        if(core.lc.containsKey(uuid)){
            core.bans.remove(uuid);
            core.lc.remove(uuid);
            NInventory.restoreInventory(target);
            target.sendMessage(getCore().getPrefix() + getMessage(LEAVE_OTHER).replace("%", p.getName()));
            p.sendMessage(getCore().getPrefix() + getMessage(CONFIRM_LEAVE).replace("%", target.getName()));
            return;
        }

        if(regions != null && !regions.allow(target)){
            if(LCModes.getMode(p) == LCModes.ADMIN){
                p.sendMessage(getCore().getPrefix() + getMessage(CONFIRM_ENTER_BYPASS).replace("%", target.getName()).replace("^", regions.getDeniedRegion(target).getId().toUpperCase()));
                target.sendMessage(getCore().getPrefix() + getMessage(ENTER_OTHER_BYPASS).replace("^", regions.getDeniedRegion(target).getId().toUpperCase()).replace("%", target.getName()));
            }else{
                p.sendMessage(getCore().getPrefix() + getMessage(REGION_BLOCKED_OTHER).replace("%", regions.getDeniedRegion(target).getId().toUpperCase()).replace("^", target.getName()));
                return;
            }
        }else{
            target.sendMessage(getCore().getPrefix() + getMessage(ENTER_OTHER).replace("%", p.getName()));
            p.sendMessage(getCore().getPrefix() + getMessage(CONFIRM_ENTER).replace("%", target.getName()));
        }

        NInventory.saveInventory(target);
        target.getInventory().clear();
        core.bans.put(uuid, new Blacklist(Blacklist.loadBans(p)));
        core.lc.put(uuid, mode);
    }

    public void lcList(Player p){

        if(!p.hasPermission("newbs.creative.list")){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PERMS));
            return;
        }

        if(getCore().lc.isEmpty()){
            p.sendMessage(getCore().getPrefix() + getMessage(LC_EMPTY));
            return;
        }
        p.sendMessage(getCore().getPrefix() + getMessage(LC_LIST));
        for(UUID uuid : getCore().lc.keySet()){
            p.sendMessage(getMessage(LC_ENTRY).replace("%",Bukkit.getPlayer(uuid).getName()).replace("^", getCore().lc.get(uuid).getType().toString().toUpperCase()));
        }
    }

    public void lcBans(Player p, String[] args){
        Player target = Bukkit.getPlayer(args[1]);
        LCModes mode = LCModes.TRIAL;

        if(!p.hasPermission("newbs.creative.bans")){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PERMS));
            return;
        }
        if(target == null){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PLAYER));
            return;
        }
        UUID uuid = target.getUniqueId();

        if(!core.lc.containsKey(uuid)){
            return;
        }
        Blacklist bl = core.bans.get(uuid);

        if(bl.getBans().isEmpty()){
            p.sendMessage(getCore().getPrefix() + getMessage(BANS_EMPTY));
            return;
        }
        p.sendMessage(getCore().getPrefix() + getMessage(BANS_LIST));

        for(Material m : bl.getBans()){
            p.sendMessage(getMessage(BANS_ENTRY).replace("%", m.toString()));
        }
    }

    public void lcHelp(Player p){

        if(!p.hasPermission("newbs.creative.help")){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PERMS));
            return;
        }

        p.sendMessage("");
        p.sendMessage("   " + BOLD + "" + AQUA + "Newbs " + BOLD + "" + BLUE + "Creative");
        p.sendMessage("");
        p.sendMessage("   " + MAGIC +"" + GREEN + "-> " + GRAY + "/lc <player>");
        p.sendMessage("   " + MAGIC + ""+ GREEN + "-> " + GRAY + "/lc list");
        p.sendMessage("   " + MAGIC + ""+ GREEN + "-> " + GRAY + "/lc bans <player>");
        p.sendMessage("");
    }

    public void lcReload(Player p) {

        if(!p.hasPermission("newbs.creative.reload")){
            p.sendMessage(getCore().getPrefix() + getMessage(INSUFFICIENT_PERMS));
            return;
        }

        core.reloadConfig();

        LCMessages.loadMessages(core);

        p.sendMessage(getCore().getPrefix() + getMessage(RELOAD));
    }

    @Override
    public void command(ConsoleCommandSender console, String[] args) {
        console.sendMessage(core.getPrefix() + getMessage(INSUFFICIENT_ENTITY));
    }

    @Override
    public List<String> tabcomplete(CommandSender sender, Command cmd, String alias, String[] args) {

        List<String> options = new ArrayList<>();

        if(!cmd.getName().equalsIgnoreCase(getCommand())) { return options; }

        if(!(sender instanceof Player)){
            return null;
        }

        Player p = (Player) sender;
        if(!p.hasPermission("newbs.creative")){ return null; }

        if(args.length == 1){
            if(p.hasPermission("newbs.creative.help")) { options.add("help"); }
            if(p.hasPermission("newbs.creative.list")) { options.add("list"); }
            if(p.hasPermission("newbs.creative.bans")) { options.add("bans"); }
            if(p.hasPermission("newbs.creative.reload")) { options.add("reload"); }

            return options;
        }
        if(args.length == 2){
            if(args[0].equalsIgnoreCase("bans")) {
                for(Player player : Bukkit.getOnlinePlayers()){
                    options.add(player.getName());
                }
            }

            return options;
        }

        return null;
    }

    @Override
    public NewbsCreative getCore() {
        return core;
    }

    @Override
    public String getCommand() {
        return "lc";
    }

    @Override
    public String getPermission() {
        return "newbs.creative";
    }

    @Override
    public List<String> getAliases() {
        return new ArrayList<>(0);
    }
}
