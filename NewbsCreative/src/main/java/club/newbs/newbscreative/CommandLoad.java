package club.newbs.newbscreative;

import club.newbs.newbscreative.api.commands.NCmdWrapper;
import club.newbs.newbscreative.commands.CommandLC;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.stream.Stream;

public class CommandLoad implements Listener {

    private final NewbsCreative core;
    private NCmdWrapper map;

    public CommandLoad(NewbsCreative core){
        this.core = core;
    }

    public void load(){
        try {
            map = new NCmdWrapper();
        } catch(NoSuchFieldException | IllegalAccessException e){
            e.printStackTrace();
        }

        Stream.of(
                new CommandLC(core)
        ).forEach(command -> map.load(command));

        Bukkit.getPluginManager().registerEvents(this, core);
    }

    public void unload(){

    }
}
