package club.newbs.newbscreative.api.lc;

import club.newbs.newbscreative.NewbsCreative;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public enum LCModes {

    //Modes

    ADMIN("newbs.admin",LCTypes.ELEVATED),
    BUILDER("newbs.builder",LCTypes.NORMAL),
    TRIAL("newbs.trial",LCTypes.LOWER);

    private final String permission;
    private final LCTypes type;

    LCModes(String permission, LCTypes type){
        this.permission=permission;
        this.type=type;
    }

    public static LCModes getMode(Player p){
        for(LCModes mode : LCModes.values()){
            if(p.hasPermission(mode.getPermission())){
                return mode;
            }
        }
        return null;
    }

    public String getPermission() {
        return permission;
    }

    public LCTypes getType() {
        return type;
    }

    public enum LCTypes {
        ELEVATED, NORMAL, LOWER
    }
}
