package club.newbs.newbscreative.api;

import club.newbs.newbscreative.NewbsCreative;
import club.newbs.newbscreative.api.lc.LCMessages;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.internal.platform.WorldGuardPlatform;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Regions {

    NewbsCreative core;
    WorldGuard wg;
    public StateFlag LC_FLAG;

    public Regions(NewbsCreative core, WorldGuard wg){
        this.core = core;
        this.wg = wg;
    }

    public void onLoad(){

        if(wg == null) { return; }

        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        try {
            // create a flag with the name "my-custom-flag", defaulting to true
            StateFlag flag = new StateFlag("lc", true);
            registry.register(flag);
            LC_FLAG = flag; // only set our field if there was no error

            Bukkit.getConsoleSender().sendMessage(core.getPrefix() + LCMessages.getMessage(LCMessages.FLAG_CREATED).replace("%", LC_FLAG.getName()));
        } catch (FlagConflictException e) {
            // some other plugin registered a flag by the same name already.
            // you can use the existing flag, but this may cause conflicts - be sure to check type
            Flag<?> existing = registry.get("lc");
            if (existing instanceof StateFlag) {
                LC_FLAG = (StateFlag) existing;

                Bukkit.getConsoleSender().sendMessage(core.getPrefix() + LCMessages.getMessage(LCMessages.FLAG_LOADED).replace("%", LC_FLAG.getName()));
            } else {
                // types don't match - this is bad news! some other plugin conflicts with you
                // hopefully this never actually happens
            }
        }
    }

    public boolean allow(Player p){
        RegionManager regionManager = wg.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
        ApplicableRegionSet set = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(p.getLocation()));

        for (ProtectedRegion region : set) {

            if (region != null){

                // here you can check for things like
                // region.getFlags();
                if(region.getFlag(LC_FLAG) == StateFlag.State.ALLOW){
                    return true;
                }else if(region.getFlag(LC_FLAG) == StateFlag.State.DENY){
                    return false;
                }
            }

        }
        return true;
    }

    public ProtectedRegion getDeniedRegion(Player p){
        RegionManager regionManager = wg.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
        ApplicableRegionSet set = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(p.getLocation()));

        for (ProtectedRegion region : set) {

            if (region != null){

                // here you can check for things like
                // region.getFlags();
                if(region.getFlag(LC_FLAG) == StateFlag.State.DENY){
                    return region;
                }
            }

        }
        return null;
    }
}
