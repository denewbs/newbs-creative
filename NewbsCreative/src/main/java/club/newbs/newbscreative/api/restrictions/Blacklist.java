package club.newbs.newbscreative.api.restrictions;

import club.newbs.newbscreative.NewbsCreative;
import club.newbs.newbscreative.api.lc.LCModes;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class Blacklist {

    private ArrayList<Material> bans;

    public Blacklist(ArrayList<Material> materials){
        this.bans = materials;
    }

    public static ArrayList<Material> loadBans(Player p){
        ArrayList<Material> bans = new ArrayList<>();
        LCModes mode = NewbsCreative.getCore().lc.get(p.getUniqueId());

        if(p.isOp() || mode == LCModes.ADMIN || p.hasPermission("*")) { return bans; }

        for(Material m : Material.values()){
            if(p.hasPermission("newbs.ban." + m.toString())){
                bans.add(m);
            }
        }
        return bans;
    }

    public void addBan(Material material){
        if(bans.contains(material)){
            return;
        }
        bans.add(material);
    }

    public void removeBan(Material material){
        if(!bans.contains(material)){
            return;
        }
        bans.remove(material);
    }

    public ArrayList<Material> getBans() {
        return bans;
    }
}
