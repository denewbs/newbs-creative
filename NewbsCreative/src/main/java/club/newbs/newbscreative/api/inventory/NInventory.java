package club.newbs.newbscreative.api.inventory;

import club.newbs.newbscreative.NewbsCreative;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class NInventory {

    public static void restoreInventory(Player p){
        p.getInventory().clear();
        YamlConfiguration c = YamlConfiguration.loadConfiguration(new File("plugins/NewbsCreative/Inventories/" + p.getName() + ".yml"));
        ItemStack[] content = ((List<ItemStack>) c.get("inventory.armor")).toArray(new ItemStack[0]);
        p.getInventory().setArmorContents(content);
        content = ((List<ItemStack>) c.get("inventory.content")).toArray(new ItemStack[0]);
        p.getInventory().setContents(content);

        p.setGameMode(GameMode.SURVIVAL);
        p.removePotionEffect(PotionEffectType.GLOWING);
    }

    public static void saveInventory(Player p){
        YamlConfiguration c = new YamlConfiguration();
        c.set("inventory.armor", p.getInventory().getArmorContents());
        c.set("inventory.content", p.getInventory().getContents());
        try {
            c.save(new File("plugins/NewbsCreative/Inventories/" + p.getName() + ".yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.setGameMode(GameMode.CREATIVE);
        p.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, Integer.MAX_VALUE, 0, false, false));
    }

    public static void unload(){
        for(UUID uuid : NewbsCreative.getCore().lc.keySet()){
            Player p = Bukkit.getPlayer(uuid);
            if(p == null){
                return;
            }
            NInventory.restoreInventory(p);
        }
        NewbsCreative.getCore().bans.clear();
        NewbsCreative.getCore().lc.clear();
    }

}
