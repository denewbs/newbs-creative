package club.newbs.newbscreative.api.lc;

import club.newbs.newbscreative.NewbsCreative;
import club.newbs.newbscreative.api.NUtils;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

import static org.bukkit.ChatColor.*;

public enum LCMessages {

    PREFIX(AQUA + "" + BOLD + "N" + DARK_AQUA + "" + BOLD + "C " + GRAY + ""),
    INSUFFICIENT_PERMS(GRAY + "Insufficient permissions to access this command."),
    INSUFFICIENT_ENTITY(GRAY + "Insufficient entity, player required."),
    INSUFFICIENT_PLAYER(GRAY + "Insufficient player, please check the name."),
    LC_EMPTY(GRAY + "LC is currently " + RED + "" + BOLD + "EMPTY"),
    BANS_EMPTY(GRAY + "BANS is currently " + RED + "" + BOLD + "EMPTY"),
    LC_LIST(GRAY + "LC is currently " + GREEN + "" + BOLD + "POPULATED"),
    BANS_LIST(GRAY + "BANS is currently " + GREEN + "" + BOLD + "POPULATED"),
    LC_ENTRY(" " + DARK_GRAY + "" + "= " + GRAY + "% " + GRAY + "- " + AQUA + "" + BOLD + "^"),
    BANS_ENTRY(" " + DARK_GRAY + "" + "= " + GRAY + "%"),
    ENTER(GRAY + "You have " + GREEN + "" + BOLD + "ENTERED " + GRAY + "creative!"),
    ENTER_BYPASS(GRAY + "You have " + GREEN + "" + BOLD + "ENTERED " + GRAY + "creative, bypassing " + AQUA + "" + BOLD + "% " + GRAY + "region."),
    LEAVE(GRAY + "You have " + RED + "" + BOLD + "LEFT " + GRAY + "creative!"),
    ENTER_OTHER(GRAY + "You have " + GREEN + "" + BOLD + "ENTERED " + GRAY + "creative, via " + GOLD + "%"),
    ENTER_OTHER_BYPASS(GRAY + "You have " + GREEN + "" + BOLD + "ENTERED " + GRAY + "creative, via " + GOLD + "%"  + GRAY + ", bypassing " + AQUA + "" + BOLD + "^"),
    LEAVE_OTHER(GRAY + "You have " + RED + "" + BOLD + "LEFT " + GRAY + "creative, via " + GOLD + "%"),
    CONFIRM_ENTER(GRAY + "You have put " + GOLD + "% " + GRAY + "into creative!"),
    CONFIRM_ENTER_BYPASS(GRAY + "You have put " + GOLD + "% " + GRAY + "into creative, bypassing " + AQUA + "" + BOLD + "^"),
    CONFIRM_LEAVE(GRAY + "You have kicked " + GOLD + "% " + GRAY + "out of creative!"),
    INVENTORIES(GRAY + "Inventories are " + RED + "" + BOLD + "NOT " + GRAY + "allowed."),
    INTERACTING(GRAY + "Interacting with Armor Stands is " + RED + "" + BOLD + "NOT " + GRAY + "allowed."),
    DROPPING(GRAY + "Dropping items is " + RED +"" + BOLD + "NOT " + GRAY + "allowed."),
    BLOCKING(GRAY + "Placing " + GOLD + "%" + GRAY + " is " + RED + "" + BOLD + "NOT " + GRAY + "allowed."),
    UPDATE(GRAY + "You are currently running version " + GOLD + "% " + GRAY + "which is " + RED + "" + BOLD + "OUTDATED"),
    FLAG_CREATED(GRAY + "Successfully created " + GOLD + "" + BOLD + "% " + GRAY + "WorldGuard flag!"),
    FLAG_LOADED(GRAY + "Successfully loaded " + GOLD + "" + BOLD + "% " + GRAY + "WorldGuard flag!"),
    SOFT_DEPEND_FOUND(GRAY + "Soft-Dependency " + GOLD + "" + BOLD + "% " + GRAY + "found."),
    SOFT_DEPEND_NOT_FOUND(GRAY + "Soft-Dependency " + GOLD + "" + BOLD + "% " + GRAY + "not found, skipping."),
    REGION_BLOCKED(GRAY + "Creative is " + RED + "" + BOLD + "NOT " + GRAY + "allowed in " + AQUA + "" + BOLD + "%"),
    REGION_BLOCKED_OTHER(GRAY + "Creative is " + RED + "" + BOLD + "NOT " + GRAY + "allowed in " + AQUA + "" + BOLD + "%" + GRAY + ", via " + GOLD + "^"),
    RELOAD(GRAY + "Plugin has been " + GOLD + "" + BOLD + "RELOADED");

    private String message;

    LCMessages(String message){
        this.message=message;
    }

    public void setMessage(String msg){
        this.message = msg;
    }

    public static String getMessage(LCMessages message){
        return message.message;
    }

    public static void loadMessages(NewbsCreative core){
        for(LCMessages msg : LCMessages.values()){
            String mesg = core.getConfig().getString("messages." + msg.toString());
            if(mesg != null){
                msg.setMessage(NUtils.translate(mesg));
            }
        }
    }

    public static void list(){
        for(LCMessages msg : LCMessages.values()){
            Bukkit.getConsoleSender().sendMessage(msg.toString());
        }
    }

}
