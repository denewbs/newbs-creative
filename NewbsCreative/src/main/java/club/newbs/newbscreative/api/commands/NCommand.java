package club.newbs.newbscreative.api.commands;

import club.newbs.newbscreative.NewbsCreative;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import java.util.List;

import static org.bukkit.ChatColor.*;

public abstract class NCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args){

        if(sender instanceof Player){

            Player p = (Player) sender;

            if(p.hasPermission(getPermission())){
                command(p, args);
                return true;
            } else{
                p.sendMessage(getCore().getPrefix() + "Insufficient permissions to access this command.");
                return false;
            }
        }else if(sender instanceof ConsoleCommandSender){

            ConsoleCommandSender cp = (ConsoleCommandSender) sender;

            command(cp, args);
            return true;
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args){
        return tabcomplete(sender, cmd, alias,args);
    }

    public abstract void command(Player player, String[] args);

    public abstract void command(ConsoleCommandSender console, String[] args);

    public abstract List<String> tabcomplete(CommandSender sender, Command cmd, String alias, String[] args);

    public abstract NewbsCreative getCore();

    public abstract String getCommand();

    public abstract String getPermission();

    public abstract List<String> getAliases();
}
