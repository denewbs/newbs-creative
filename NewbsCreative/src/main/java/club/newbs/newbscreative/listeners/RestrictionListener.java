package club.newbs.newbscreative.listeners;

import club.newbs.newbscreative.NewbsCreative;
import club.newbs.newbscreative.api.lc.LCModes;
import club.newbs.newbscreative.api.restrictions.Blacklist;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import java.util.ArrayList;

import static club.newbs.newbscreative.api.lc.LCMessages.*;

public class RestrictionListener implements Listener {

    @EventHandler
    public void onPickup(EntityPickupItemEvent e){
        if(NewbsCreative.getCore().lc.containsKey(e.getEntity().getUniqueId())){
            LCModes mode = NewbsCreative.getCore().lc.get(e.getEntity().getUniqueId());
            if(!(mode.getType() == LCModes.LCTypes.ELEVATED)){
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInventory(InventoryOpenEvent e){
        if(NewbsCreative.getCore().lc.containsKey(e.getPlayer().getUniqueId())){
            LCModes mode = NewbsCreative.getCore().lc.get(e.getPlayer().getUniqueId());
            if(!(mode.getType() == LCModes.LCTypes.ELEVATED)){
                if(!(e.getInventory().getType() == InventoryType.PLAYER)){
                    e.setCancelled(true);
                    e.getPlayer().sendMessage(NewbsCreative.getCore().getPrefix() + getMessage(INVENTORIES));
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerArmorStandManipulateEvent e){
        if(NewbsCreative.getCore().lc.containsKey(e.getPlayer().getUniqueId())){
            LCModes mode = NewbsCreative.getCore().lc.get(e.getPlayer().getUniqueId());
            if(mode.getType() == LCModes.LCTypes.LOWER){
                e.setCancelled(true);
                e.getPlayer().sendMessage(NewbsCreative.getCore().getPrefix() + getMessage(INTERACTING));
            }
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e){
        if(NewbsCreative.getCore().lc.containsKey(e.getPlayer().getUniqueId())){
            LCModes mode = NewbsCreative.getCore().lc.get(e.getPlayer().getUniqueId());
            if(!(mode.getType() == LCModes.LCTypes.ELEVATED)){
                e.setCancelled(true);
                e.getPlayer().sendMessage(NewbsCreative.getCore().getPrefix() + getMessage(DROPPING));
            }
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e){
        if(NewbsCreative.getCore().lc.containsKey(e.getPlayer().getUniqueId())){
            Blacklist bans = NewbsCreative.getCore().bans.get(e.getPlayer().getUniqueId());
            for(Material m : bans.getBans()){
                if(e.getBlockPlaced().getType() == m){
                    e.getPlayer().sendMessage(NewbsCreative.getCore().getPrefix() + getMessage(BLOCKING).replace("%", e.getBlockPlaced().getType().toString()));
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player){
            Player hitter = (Player) e.getDamager();
            if(LCModes.getMode(hitter) != LCModes.ADMIN && NewbsCreative.getCore().lc.containsKey(hitter.getUniqueId())){
                if(e.getEntity() instanceof Player){
                    e.setCancelled(true);
                }
            }
        }
    }

}
