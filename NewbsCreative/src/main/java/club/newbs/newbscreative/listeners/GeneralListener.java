package club.newbs.newbscreative.listeners;

import club.newbs.newbscreative.NewbsCreative;
import club.newbs.newbscreative.api.inventory.NInventory;
import club.newbs.newbscreative.api.lc.LCMessages;
import club.newbs.newbscreative.api.lc.LCModes;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;

import static org.bukkit.ChatColor.*;

public class GeneralListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        File file = new File("plugins/NewbsCreative/Inventories/" + p.getName() + ".yml");

        if(NewbsCreative.getCore().lc.containsKey(p.getUniqueId())){
            if(file.exists()){
                NInventory.saveInventory(p);
            }
        }

        if(NewbsCreative.getCore().outdated){
            if(p.isOp()){
                TextComponent comp = new TextComponent(NewbsCreative.getCore().getPrefix() + LCMessages.getMessage(LCMessages.UPDATE).replace("%", NewbsCreative.getCore().getDescription().getVersion()));
                comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(GRAY + "Click to visit Plugin Page!")));
                comp.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/nc-limited-creative.80800/"));

                p.spigot().sendMessage(comp);
            }
        }

    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        Player p = e.getPlayer();
        if(LCModes.getMode(p) != LCModes.ADMIN && NewbsCreative.getCore().lc.containsKey(p.getUniqueId())){
            NewbsCreative.getCore().bans.remove(p.getUniqueId());
            NewbsCreative.getCore().lc.remove(p.getUniqueId());

            NInventory.restoreInventory(p);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e){
        Player p = e.getPlayer();
        Block b = e.getBlockPlaced();
        Location loc = b.getLocation();
        if(LCModes.getMode(p) != LCModes.ADMIN && NewbsCreative.getCore().lc.containsKey(p.getUniqueId())){
            NewbsCreative.getCore().blocks.add(b);
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e){
        Player p = e.getPlayer();
        Block b = e.getBlock();

        if(NewbsCreative.getCore().blocks.contains(b)){
            e.setDropItems(false);
        }
    }

}
