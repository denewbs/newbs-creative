package club.newbs.newbscreative;

import club.newbs.newbscreative.api.Regions;
import club.newbs.newbscreative.api.Updater;
import club.newbs.newbscreative.api.inventory.NInventory;
import club.newbs.newbscreative.api.lc.LCMessages;
import club.newbs.newbscreative.api.lc.LCModes;
import club.newbs.newbscreative.api.restrictions.Blacklist;
import club.newbs.newbscreative.listeners.GeneralListener;
import club.newbs.newbscreative.listeners.RestrictionListener;
import com.sk89q.worldguard.WorldGuard;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

public final class NewbsCreative extends JavaPlugin {

    public boolean outdated = false;

    private final CommandLoad cmdmap = new CommandLoad(this);
    public Regions regions;
    private static NewbsCreative core;

    public HashMap<UUID, LCModes> lc = new HashMap<>();
    public HashMap<UUID, Blacklist> bans = new HashMap<>();
    public List<Block> blocks = new ArrayList<Block>();

    private String prefix;

    @Override
    public void onEnable() {
        core = this;

        cmdmap.load();
        update();
        metrics();
        registerEvents();
    }

    @Override
    public void onDisable() {
        NInventory.unload();
    }

    private void registerEvents() {
        Stream.of(
                new GeneralListener(),
                new RestrictionListener()
        ).forEach(event -> Bukkit.getPluginManager().registerEvents(event, this));
    }

    @Override
    public void onLoad() {

        config();

        prefix = LCMessages.getMessage(LCMessages.PREFIX);

        // ... do your own plugin things, etc
        Plugin worldguard = getServer().getPluginManager().getPlugin("WorldGuard");

        if(!(worldguard == null)){
            Bukkit.getConsoleSender().sendMessage(getPrefix() + LCMessages.getMessage(LCMessages.SOFT_DEPEND_FOUND).replace("%", "WorldGuard"));

            regions = new Regions(this, WorldGuard.getInstance());
            regions.onLoad();
        }else { Bukkit.getConsoleSender().sendMessage(getPrefix() + LCMessages.getMessage(LCMessages.SOFT_DEPEND_NOT_FOUND).replace("%", "WorldGuard")); }
    }

    private void update(){
        new Updater(this, 80800).getVersion(version ->{
            if(!this.getDescription().getVersion().equalsIgnoreCase(version)){
                Bukkit.getConsoleSender().sendMessage(getPrefix() + LCMessages.getMessage(LCMessages.UPDATE).replace("%", this.getDescription().getVersion()));
                outdated = true;
            }else{
                outdated = false;
            }
        });
    }

    private void metrics(){
        int pluginID = 11992;
        Metrics metrics = new Metrics(this,pluginID);
    }

    private void config(){
        if(!this.getDataFolder().exists()){
            this.getDataFolder().mkdirs();
        }
        getConfig().options().copyDefaults(true);
        saveConfig();

        LCMessages.loadMessages(this);
    }

    public String getPrefix() {
        return prefix;
    }

    public static NewbsCreative getCore() {
        return core;
    }

    public Regions getRegions() {
        return regions;
    }
}
